# Mobile courses Server

Mobile Courses Project for LP student at University of Nice Sophia Antipolis.

## Install Node.js

Go to (node.js site)[https://nodejs.org/] and download package for last version.

## Install Server

```bash
cd mobile-courses-server
npm install
```
Server is based on Express framework, that is why it is installed.

## Start server

```bash
node server
```

You can then access it through:

```
http://localhost:8990/
```
## Deployed on Heroku

http://mobile-courses-server.herokuapp.com/courses

## Example of Web Client

[Rendu Johann (JS)](https://gitlab.com/jbernard2017/TD2_BDD)
[Rendu Rémy (PHP)](https://gitlab.com/remy554/td2_bdd_client)
[Rendu Anthony C. (PHP)](https://gitlab.com/HeavenMetronom/TD2-CUTTIVET-API)

Inspired from this project: [Github project Henri Potier](https://github.com/Nilhcem/xebia-android-hp-kotlin)
